#   ___   __ __  _____  ____   ______  __ __ 
#  /   \ |  |  ||     ||    \ |      ||  |  |
# |     ||  |  ||__/  ||  o  )|      ||  |  |
# |  O  ||  |  ||   __||     ||_|  |_||  ~  |
# |     ||  :  ||  /  ||  O  |  |  |  |___, |
# |     ||     ||     ||     |  |  |  |     |
#  \___/  \__,_||_____||_____|  |__|  |____/ 
# 
#           ouzb65ty@protonmail.ch

CC = gcc
NAME = freegraph
CFLAGS = -Wall -Wextra -Werror -pedantic
LDFLAGS = -leditline
INC = -I inc
SRC = $(wildcard *.c)
OBJ = $(SRC:.c=.o)
AR = ar rcs

all: $(NAME)

$(NAME): $(OBJ)
	$(CC) -o $@ $^ $(CFLAGS) $(LDFLAGS) $(INC)

%.o: %.c
	$(CC) -o $@ -c $< $(LDFLAGS) $(INC)

.PHONY: clean mrproper

clean:
	@rm -rf *.o

mrproper: clean
	@rm -rf $(NAME)

