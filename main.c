/*
 *   ___   __ __  _____  ____   ______  __ __ 
 *  /   \ |  |  ||     ||    \ |      ||  |  |
 * |     ||  |  ||__/  ||  o  )|      ||  |  |
 * |  O  ||  |  ||   __||     ||_|  |_||  ~  |
 * |     ||  :  ||  /  ||  O  |  |  |  |___, |
 * |     ||     ||     ||     |  |  |  |     |
 *  \___/  \__,_||_____||_____|  |__|  |____/ 
 * 
 *            ouzb65ty@protonmail.ch
 */

#include "inc/freegraph.h"

static int
repl(void) {

    char *input = NULL;
    lex_t *lex = NULL;
    synt_t *synt = NULL;

    while ((input = readline("FreeGraph> ")) != NULL) {
        lex = lexer(input);
        print_lex(lex);
	synt = lparser(lex);
	//print_synt(synt);
        free_lex(lex);
        free(input);
    }
    return (0);
}

static void
print_help(void) {

    printf("\n\t--------- FreeGraph ---------\n\toptions :\n\t-h\tDisplay this help\n\t-f\tRead from file\n\t--------------------------\n\n");
    return ;
}

static int
feval(const char *filename) {

    FILE *fp;
    char *line = NULL;
    size_t len = 0;
    ssize_t ret;

    fp = fopen(filename, "r");
    if (fp == NULL)
        return (-1);
    ret = getline(&line, &len, fp);
    do {
        lexer(line); /* Line Evaluation */
        ret = getline(&line, &len, fp);
    } while (ret != -1);
    if (line) free(line);
    fclose(fp);
    return (0);
}

int
main(int argc, char **argv) {

    int res = 0;

    if (argc == 3 && strcmp(argv[1], "-f") == 0) {
        res = feval(argv[2]); /* File Evaluation */
        if (res == -1) /* ENOENT */
            fprintf(stderr, "FreeGraph: error: %s\n", strerror(ENOENT));
    } else if (argc == 2 && strcmp(argv[1], "-h") == 0) {
        print_help();
    } else if (argc == 1) {
        repl();
    } else {
        fprintf(stderr, "FreeGraph: error: %s\n", strerror(EINVAL));
    }
    return (EXIT_FAILURE);
}
